// #include<stdio.h>
// int main (){
//     int n;
//     scanf("%d",&n);
//     int a;
//     int max=0;
//     for(int i=1; i<=n; i++){
//         scanf("%d",&a);
//         if(a>max){
//             max=a;
//         }
//     }
//     printf("%d",max);
//     return 0;
// }

#include<stdio.h>
#include<limits.h>
int main (){
    int n;
    scanf("%d",&n);
    int a;
    int max=INT_MIN, min=INT_MAX;
    for(int i=1; i<=n; i++){
        scanf("%d",&a);
        if(a>max){
            max=a;
        }
        if(a<min){
            min=a;
        }
    }
    printf("max : %d min : %d",max, min);
    return 0;
}