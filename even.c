// #include<stdio.h>
// int main (){
//     int i, n;
//     scanf("%d",&n);
//     for(i=2; i<=n; i+=2)
//     printf("%d\n",i);
//     return 0;
// }

#include <stdio.h>
int main()
{
    int n;
    scanf("%d", &n);
    if (n == 1)
    {
        printf("-1\n");
    }
    else
    {
        for (int i = 2; i <= n; i += 2)
        {
            if (i % 2 == 0)
            {
                printf("%d\n", i);
            }
        }
    }
    return 0;
}