// #include<stdio.h>
// int main (){
//     int i=10;
//     // i++;  post-increment
//     // ++i;  pre-increment
//     // int x = i++;
//     int y = ++i;
//     printf ("y= %d, i = %d",y,i);
//     return 0;
// }


//decrement
#include<stdio.h>
int main (){
    int i=10;
    // i--;  post-decrement
    // --i;  pre-decrement
    // int x = i--;
    int y = --i;
    printf ("y= %d, i = %d",y,i);
    return 0;
}